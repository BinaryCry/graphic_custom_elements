// import './custom/art';

const c = console.log;

////////////////
class CstBtn extends HTMLButtonElement {
  constructor() {
    super();
    this._count = 0;
    this.textContent = this.getInnerText();
    this.addEventListener('click', _ => {
      this._count++;
      this.textContent = this.getInnerText();
    }, false);
  }

  getInnerText() {
    return `Now clicked ${this.count} times`;
  }

  set count(value) {
    c(false, 'value is incapsulated!');
  }

  get count() {
    return this._count;
  }
}

customElements.define('cst-btn', CstBtn, { extends: 'button' });

/////////////////////
class Art extends HTMLElement {
  constructor() {
    super();
    this._name = 'Bee';
    this.count = 0;
    
    this.shadow = this.attachShadow({ mode: 'closed' });
  }

  static get observedAttributes() { return ["title"]; }

  set name(value) {
    this._name = value;
  }

  get name() {
    return this._name;
  }

  attributeChangedCallback(name, oldValue, newValue) {
    debugger
  }

  connectedCallback() {
    this.count++;
    c(`found ${this.count}`);
    const style = document.createElement('style');
    style.textContent = `
      button {
        background-color: #9c1a9c;
        color: white;
        font-family: 'Courier New', monospace;
        padding: 5px 10px;
      }
    `;
    this.shadow.appendChild(style);
    this.shadow.appendChild(document.createElement('button', { is: 'cst-btn' }));
    c(this.getAttribute('id'));
  }

  disconnectedCallback() {
    c('removed');
  }
}

customElements.define('art-cst', Art);