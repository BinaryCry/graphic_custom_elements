import "./style.css";
import * as THREE from "three";

const selector = "app";
const canvas = document.getElementById(selector);
const camera = new THREE.PerspectiveCamera(
  50,
  canvas.clientWidth / canvas.clientHeight,
  0.1,
  1000
);
const scene = new THREE.Scene();
scene.background = new THREE.Color(0x1a323e);
const renderer = new THREE.WebGLRenderer({
  canvas
});

const geometry = new THREE.BoxGeometry(4, 4, 7);
const material = new THREE.MeshPhongMaterial({
  color: 0xff3eee
});
material.opacity = 0.85;
material.transparent = true;

renderer.setSize(canvas.clientWidth, canvas.clientHeight);
renderer.setClearColor(0x000000);
renderer.shadowMap.enabled = true;
renderer.shadowMap.type = THREE.PCFSoftShadowMap; // default THREE.PCFShadowMap

const cube = new THREE.Mesh(geometry, material);
cube.castShadow = true; //default is false
cube.receiveShadow = false; //default
scene.add(cube);

camera.position.x = 0;
camera.position.y = 0;
camera.position.z = 22.5;

cube.rotation.x += 0.25;
cube.rotation.y += 0.5;

const light = new THREE.PointLight(0xffffff, 1, 1000);
light.position.set(15, 25, 20);
light.castShadow = true;            // default false
//Set up shadow properties for the light
light.shadow.mapSize.width = 512;  // default
light.shadow.mapSize.height = 512; // default
light.shadow.camera.near = 0.5;       // default
light.shadow.camera.far = 500      // default
scene.add(light);

const geometry2 = new THREE.BoxGeometry(3, 3, 3);
const material2 = new THREE.MeshPhongMaterial({
  color: 0x5fce63
});
const cube2 = new THREE.Mesh(geometry2, material2);
const cube3 = new THREE.Mesh(geometry2, material2);
cube2.receiveShadow = true; //default
cube3.receiveShadow = true; //default

cube2.rotation.x += 0.25;
cube2.rotation.y += 0.5;

cube3.rotation.x += 0.25;
cube3.rotation.y += -0.5;

scene.add(cube2);
scene.add(cube3);

cube.position.setY(2.5)
cube2.position.setY(-2.5)
cube2.position.setX(-3)

cube3.position.setY(-4)
cube3.position.setX(1.75)

// const axesHelper = new THREE.AxesHelper( 5 );
// scene.add(axesHelper)

function animate() {
  // cube.rotation.x += 0.02;
  // cube.rotation.y += 0.02;
  cube.rotation.z -= 0.02;
  requestAnimationFrame(animate);
  renderer.render(scene, camera);
}
animate();
