import "./style.css";
import * as THREE from "three";

const selector = "app";
const canvas = document.getElementById(selector);
const renderer = new THREE.WebGLRenderer({
  canvas
});
var camera = new THREE.PerspectiveCamera(
  60,
  window.innerWidth / window.innerHeight,
  1,
  500
);
camera.position.set(0, 0, 100);
camera.lookAt(new THREE.Vector3(0, 0, 0));

var material = new THREE.LineBasicMaterial({ color: 0xff3eee });
var geometry = new THREE.Geometry();
geometry.vertices.push(new THREE.Vector3(-10, 0, 0));
geometry.vertices.push(new THREE.Vector3(0, 10, 0));
geometry.vertices.push(new THREE.Vector3(10, 0, 0));
geometry.vertices.push(new THREE.Vector3(-15, 5, 0));
geometry.vertices.push(new THREE.Vector3(15, 5, 0));
geometry.vertices.push(new THREE.Vector3(-10, 0, 0));

var line = new THREE.Line(geometry, material);

const scene = new THREE.Scene();
scene.background = new THREE.Color(0x1a323e);

scene.add(line);

var arcShape = new THREE.Shape();
// arcShape.moveTo(50, 10);
arcShape.absarc(0, 0, 30, 0, Math.PI * 2, false);

var holePath = new THREE.Path();
// holePath.moveTo(20, 10);
holePath.absarc(1, 1, 10, 0, Math.PI * 2, true);
arcShape.holes.push(holePath);

var geometry2 = new THREE.ShapeBufferGeometry(arcShape);
var material2 = new THREE.MeshPhongMaterial({
  color: 0xff3eee,
  side: THREE.DoubleSide
});

var mesh = new THREE.Mesh(geometry2, material2);

scene.add(mesh);

var light = new THREE.AmbientLight( 0x404040 ); // soft white light
scene.add( light );

function animate() {
  // mesh.rotation.z -= 0.1;
  mesh.rotation.y -= 0.05;
  requestAnimationFrame(animate);
  renderer.render(scene, camera);
}
animate();

window.addEventListener("keypress", e => {
  const key = e.key.toLowerCase();
  switch (key) {
    case "w":
      line.position.setY(line.position.y + 1);
      break;
    case "s":
      line.position.setY(line.position.y - 1);
      break;
    case "a":
      line.position.setX(line.position.x - 1);
      break;
    case "d":
      line.position.setX(line.position.x + 1);
      break;
    default:
      null;
  }
});
