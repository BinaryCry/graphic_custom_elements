import './new.css';
import './anim.css';
import './grid.css';

import './custom/img-frame/';

import "./custom/todo-item";
import './custom/learn';
import './custom/wrap-tag';
import './custom/theme';
import './custom/theme/index.css';

// import './customElements';

import { initShaders } from './helpers';
import * as v_point from './shaders/point/point.vert';
import * as f_point from './shaders/point/point.frag';

(function(canvas) {
  if (!canvas) {
    return;
  }

  const gl = canvas.getContext('webgl');

  if (!gl) {
    console.log('No context');
    return;
  }

  gl.clearColor(0, 0, 0, 1);
  gl.clear(gl.COLOR_BUFFER_BIT);

  const compiled_shaders = initShaders(gl, v_point, f_point);

  if (compiled_shaders) {
    const shaderProgram = gl.createProgram();

    gl.attachShader(shaderProgram, compiled_shaders[0]);
    gl.attachShader(shaderProgram, compiled_shaders[1]);

    gl.linkProgram(shaderProgram);
    gl.useProgram(shaderProgram);

    gl.drawArrays(gl.POINTS, 0, 1);
  } else {
    console.error('Check the arguments');
  }
})(document.getElementById('app'));

