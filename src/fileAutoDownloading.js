// simple file downloading (blob)
const fileName = '3.jpeg';
document.getElementById('b').addEventListener('click', () => {
  fetch(`/${fileName}`)
		.then(res => { if (res.status === 200) return res.blob(); throw new Error(`Bad status: ${res.status}`) })
		.then(blob => URL.createObjectURL(blob))
		.then((blob) => {
      const a = document.createElement('A');
      a.style.display = 'none';
      a.download = fileName;
			a.href = blob;
      document.getElementById('d').appendChild(a);
			setTimeout(() => {
        a.dispatchEvent(new MouseEvent('click'));
        URL.revokeObjectURL(blob);
        a.remove();
      }, 0);
		});
});