class Theme extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {}

  attributeChangedCallback(name, oldVal, newVal) {
    switch (name) {
      case "light":
        break;
      case "dark":
        break;
      default:
        break;
    }
  }

  static get observedAttributes() {
    return ["dark", "light"];
  }
}
