const tmpl = document.createElement("template");
tmpl.innerHTML = `
  <style>
    :host * {
      box-sizing: border-box;
    }
    :host {
      font-family: sans-serif;
      display: block;
    }
    .group {
      margin: 0 0 0 1rem;
    }
    input, button {
      border-radius: 2px;
      vertical-align: text-top;
    }
    input {
      outline: none;
      font-size: 1.5rem;
    }
    button {
      padding: 7px 10px;
    }
    .list {
      list-style: none;
    }
  </style>  
  <div class="group">
    <input class="todo" type="text">&emsp;
    <button class="add">Button</button>&emsp;
    <button class="clean">Clean</button>
  </div>
  
  <ul class="list">
    
  </ul>
`;

class Learn extends HTMLElement {
  constructor() {
    super();

    this._shadowRoot = this.attachShadow({ mode: "open" });
    this._shadowRoot.appendChild(tmpl.content.cloneNode(true));
    this.$list = this._shadowRoot.querySelector(".list");
    this.$addBtn = this._shadowRoot.querySelector(".add");
    this.$cleanBtn = this._shadowRoot.querySelector(".clean");
    this.$input = this._shadowRoot.querySelector(".todo");

    this.$addBtn.addEventListener("click", this._addTodo.bind(this));
    this.$cleanBtn.addEventListener("click", this._fflush.bind(this));
  }

  _addTodo() {
    if (this.$input.value.length > 0) {
      this._todos = [...this._todos, { text: this.$input.value, done: false }];
      this._renderList();
      this.$input.value = null;
    }
  }

  _renderList() {
    this.$list.innerHTML = null;
    this._todos.forEach((todo, index) => {
      const el = document.createElement("todo-item");
      el.setAttribute("text", todo.text);
      if (todo.done) {
        el.setAttribute("checked", "");
      }
      el.setAttribute("index", index);
      el.addEventListener("onRemove", this._removeTodo.bind(this));
      el.addEventListener("onToggle", this._toggleTodo.bind(this));
      this.$list.appendChild(el);
    });
  }

  _toggleTodo(e) {
    this._todos[e.detail].done = !this._todos[e.detail].done;
    this._renderList();
  }

  _fflush() {
    this._todos = [];
    this._renderList();
  }

  _removeTodo(e) {
    this._todos = [
      ...this._todos.slice(0, e.detail),
      ...this._todos.slice(e.detail + 1)
    ];
    this._renderList();
  }

  get todos() {
    return this._todos;
  }

  set todos(arg) {
    this._todos = arg;
    this._renderList();
  }

  connectedCallback() {
    console.log("connected");
  }

  disconnectedCallback(e) {
    console.log("disconnected", e);
  }

  adoptedCallback(e) {
    console.log("adopted!", e);
  }
}

customElements.define("learn-tag", Learn);
const el = document.createElement("learn-tag");
document.getElementsByClassName("main")[0].append(el);
const lt = document.getElementsByTagName("learn-tag");
// els[0].remove();

lt[0].todos = [
  { text: "By a coffe", done: false },
  { text: "Send an email", done: true },
  { text: "Make a breakfast", done: false },
];
