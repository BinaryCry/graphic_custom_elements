const template = document.createElement("template");
template.innerHTML = `
    <style>
        :host * {
            box-sizing: border-box;
        }
        :host {
            font-family: sans-serif;
            display: block;
        }
        .completed {
            text-decoration: line-through;
        }
        .item {
            padding: 5px 0px;
        }
    </style>
    <li class="item">
        <input class="check" type="checkbox">
        <label class="lb"></label>
        <button class="btn">X</button>
    </li>
`;

class TodoItem extends HTMLElement {
  constructor() {
    super();

    this._shadowRoot = this.attachShadow({ mode: "open" });
    this._shadowRoot.appendChild(template.content.cloneNode(true));

    this.$li = this._shadowRoot.querySelector(".item");
    this.$check = this._shadowRoot.querySelector(".check");
    this.$label = this._shadowRoot.querySelector(".lb");
    this.$btn = this._shadowRoot.querySelector(".btn");

    this.$btn.addEventListener("click", _ => {
      this.dispatchEvent(new CustomEvent("onRemove", { detail: this.index }));
    });

    this.$check.addEventListener("click", _ => {
      this.dispatchEvent(new CustomEvent("onToggle", { detail: this.index }));
    });
  }

  connectedCallback() {
    if (!this.hasAttribute("text") || this.getAttribute("text").length === 0) {
      this.setAttribute("text", "placeholder");
    }

    this._renderTodo();
  }

  _renderTodo() {
    if (this.hasAttribute("checked")) {
      this.$li.classList.add("completed");
      this.$check.setAttribute("checked", "");
    } else {
      this.$li.classList.remove("completed");
      this.$check.removeAttribute("checked");
    }

    this.$label.innerHTML = this._text;
  }

  attributeChangedCallback(name, oldVal, newVal) {
    if (name === "text") {
      this._text = newVal;
    }

    if (name === "checked") {
      this._checked = this.hasAttribute("checked");
    }

    if (name === "index") {
      this._index = parseInt(newVal);
    }
  }

//   get checked() {
//     return this.hasAttribute("checked");
//   }

//   set checked(val) {
//     if (val) {
//       this.setAttribute("checked", "");
//     } else {
//       this.removeAttribute("checked");
//     }
//   }

  get index() {
    return this._index;
  }

  set index(i) {
    this.setAttribute("index", i);
  }

  static get observedAttributes() {
    return ["text", "checked", "index"];
  }
}

window.customElements.define("todo-item", TodoItem);
