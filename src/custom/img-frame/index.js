class ImgFrame extends HTMLElement {
  constructor() {
    super();

    this.delay = 0;
    this.shadow = this.attachShadow({ mode: 'closed' });
    const div = document.createElement('DIV');
    div.id = 'thumb';
    const style = document.createElement('STYLE');
    style.textContent = `
      :host {
        background-color: #222111ff;
        height: 200px;
        width: 300px;
        display: flex;
        align-items: center;
      }
      @keyframes rot {
        0% {
          transform: rotate(0deg) scale(.5);
        }
        100% {
          transform: rotate(360deg) scale(1);
        }
      }
      
      @keyframes fade {
        from {
          opacity: .15;
        }
        to {
          opacity: 1;
        }
      }
      
      @keyframes rect {
        from {
          border-radius: 50%;
        }
      }
      
      #thumb {
        width: 100px;
        height: 100px;
        margin: 45px 0 0 95px;
        background-color: #E91E63;
        border-radius: 8.75%;
        background-image: url('/sc.png');
        background-size: 65%;
        background-repeat: no-repeat;
        background-position: 18px 16px;
        box-shadow: 0 0 100px 0px teal;
      
        animation:
          rot 1s infinite alternate,
          fade 1s infinite alternate,
          rect 1s infinite alternate;
        }
        #img {
          height: 200px;
        }
      `;

    const wrap = document.createElement('DIV');
    wrap.style.border = "1px solid transparent"
    wrap.appendChild(div);
    this.shadow.appendChild(style);
    this.shadow.appendChild(wrap);
  }

  connectedCallback() {
    const img = document.createElement('IMG');
    img.src = `/${this.getAttribute('pic')}`;
    img.id = 'img';
    img.onload = _ => {
      setTimeout(_ => {
        this.shadow.getElementById('thumb').remove();
        this.shadow.appendChild(img);
      }, this.delay);
    }
  }
}

customElements.define('img-frame', ImgFrame);