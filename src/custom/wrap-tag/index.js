const template = document.createElement("template");

class WrapTag extends HTMLElement {
  constructor() {
    super();

    const themeType = this.parentElement.hasAttribute("dark")
      ? "dark"
      : "light";

    template.innerHTML = `
    <style>
        :host {
            display: block;
            margin: 1em;
        }
        span {
            color: var(--color-${themeType});
            background-color: var(--bg-${themeType});
            padding: var(--padding);
            border-radius: var(--br);
        }
    </style>
    <span>123</span>
`;

    this._shadowDom = this.attachShadow({ mode: "open" });
    this._shadowDom.appendChild(template.content.cloneNode(true));
  }

  connectedCallback() {}
}

customElements.define("wrap-tag", WrapTag);
