import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/**
 * `art-element`
 * 
 *
 * @artElement
 * @polymer
 * @demo demo/index.html
 */
class ArtElement extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <h2>Hello [[prop1]]!</h2>
    `;
  }
  static get properties() {
    return {
      prop1: {
        type: String,
        value: 'art-element',
      },
    };
  }
}

window.customElements.define('art-element', ArtElement);
