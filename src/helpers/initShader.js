import { VERTEX_SHADER, FRAGMENT_SHADER } from '../const';

export const initShader = (ctx, shaderCode, type) => {
  const shader = ctx.createShader(ctx[type]);
  ctx.shaderSource(shader, shaderCode);
  ctx.compileShader(shader);
  return shader;
};

export const initShaders = (ctx, vShaderSource, fShaderSource) =>
  ctx.canvas && vShaderSource && fShaderSource
    ? [
        initShader(ctx, vShaderSource, VERTEX_SHADER),
        initShader(ctx, fShaderSource, FRAGMENT_SHADER)
      ]
    : null;